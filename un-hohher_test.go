package main

import (
	"errors"
	"io/fs"
	"os"
	"reflect"
	"testing"
)

const inputFilePath = "./testResources/test-input.srt"
const expectedFilePath = "./testResources/test-expected.srt"
const testOutputFileName = "./test-input.un-hohhed.srt"

func TestSomethingIsWritten(t *testing.T) {
	err := os.Remove(testOutputFileName)
	if err != nil && !errors.Is(err, fs.ErrNotExist) {
		t.Fatalf("Could not remove output file: %v", err)
	}

	doTheMagic(inputFilePath)

	outputFile, err := os.ReadFile(testOutputFileName)
	if err != nil {
		t.Fatalf("No file written: %v", err)
	}
	expectedFile, err := os.ReadFile(expectedFilePath)
	if err != nil {
		t.Fatalf("No file written: %v", err)
	}
	actualText := string(outputFile)
	expectedText := string(expectedFile)

	if actualText != expectedText {
		t.Fatalf("Mismatch \n-Expected-\n%v\n-Actual-\n%v", expectedText, actualText)
	}
}

func TestSplittingToArrayWorks(t *testing.T) {
	input := `5
00:01:25,546 --> 00:01:27,441
Operator:
Main screen turn on.

6
00:01:28,579 --> 00:01:31,059
CATS:
All your base are belong to us.`

	expected := []string{
		`5
00:01:25,546 --> 00:01:27,441
Operator:
Main screen turn on.`,
		`6
00:01:28,579 --> 00:01:31,059
CATS:
All your base are belong to us.`}

	actual := splitByNewlines(input)

	if !reflect.DeepEqual(actual, expected) {
		t.Fatalf("Things weren't equal. Got: %v", actual)
	}
}

func TestSubLineCanBeOutput(t *testing.T) {
	expected := `6
00:01:28,579 --> 00:01:31,059
CATS:
All your base are belong to us.`

	theSlice := []string{"CATS:", "All your base are belong to us."}
	subLine := SubLine{OriginalNumber: "1",
		TimeCode: "00:01:28,579 --> 00:01:31,059",
		Text:     theSlice}
	actual := subLine.toString(6)
	if expected != actual {
		t.Fatalf("Expected: %v \nActual: %v", expected, actual)
	}
}

func TestSubLineCanTellIfItIsNotEffects(t *testing.T) {
	theSlice := []string{"CATS:", "All your base are belong to us."}
	subLine := SubLine{OriginalNumber: "6",
		TimeCode: "00:01:28,579 --> 00:01:31,059",
		Text:     theSlice}
	if subLine.isEffect() {
		t.Fatalf("Should have not been just effects: %v", subLine)
	}
}

func TestSubLineCanTellIfItIsJustEffects(t *testing.T) {
	theSlice := []string{"[Music]"}
	subLine := SubLine{OriginalNumber: "1",
		TimeCode: "00:00:44,081 --> 00:00:50,238",
		Text:     theSlice}
	if !subLine.isEffect() {
		t.Fatalf("Should have been just effects: %v", subLine)
	}
}

/*
func TestSubLineCanTellIfItIsHasBracketsButIsNotEffects(t *testing.T) {
	theSlice := []string{"[CATS:]", "All your [base] are belong ", "[to us.]"}
	subLine := SubLine{OriginalNumber: "6",
		TimeCode: "00:01:28,579 --> 00:01:31,059",
		Text:     theSlice}
	if subLine.isEffect() {
		t.Fatalf("Should have not been just effects: %v", subLine)
	}
}
*/

func TestSubLineCanBeParsed(t *testing.T) {
	original := `6
00:01:28,579 --> 00:01:31,059
CATS:
All your base are belong to us.`

	theSlice := []string{"CATS:", "All your base are belong to us."}
	expected := SubLine{OriginalNumber: "6",
		TimeCode: "00:01:28,579 --> 00:01:31,059",
		Text:     theSlice}
	actual := parseString(original)
	if expected.OriginalNumber != actual.OriginalNumber {
		t.Fatalf("OriginalNumer mismatch - Expected: %v \nActual: %v", expected, actual)
	}
	if expected.TimeCode != actual.TimeCode {
		t.Fatalf("TimeCode mismatch - Expected: %v \nActual: %v", expected, actual)
	}
	if !reflect.DeepEqual(actual.Text, expected.Text) {
		t.Fatalf("Text mismatch - Expected: %v \nActual: %v", expected, actual)
	}
}

func TestNewLinesAreSkipped(t *testing.T) {
	input := []string{"foo", "", "bar"}
	expected := []string{"foo", "bar"}

	actual := filterEmpties(input)

	if !reflect.DeepEqual(actual, expected) {
		t.Fatalf("Text mismatch - Expected: %v \nActual: %v", expected, actual)
	}

}
