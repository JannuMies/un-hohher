package main

import (
	"strconv"
	"strings"
)

type SubLine struct {
	OriginalNumber string
	TimeCode       string
	Text           []string
}

func (s SubLine) toString(newNumber int) string {
	output := []string{strconv.Itoa(newNumber), s.TimeCode}
	output = append(output, s.Text...)
	return strings.Join(output, "\n")
}

func (s SubLine) isEffect() bool {
	asOneString := strings.TrimSpace(strings.Join(s.Text, ""))
	if strings.Index(asOneString, "[") == -1 {
		return false
	}
	if (strings.Index(asOneString, "[") == 0) && (strings.LastIndex(asOneString, "]") == len(asOneString)-1) {
		return true
	}
	return false
}
