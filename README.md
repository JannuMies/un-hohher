# Thing that strips Hard-of-Hearing blocks out of .srt files

Things working:
 * Reading and writing files
 * Crude stripping

Things not working:
 * Support for Windows \r\n newlines
 * Dynamically outputting a correct file name.
 * Support for complex text, instead of matching just start and end brackets
 
As a non-native english speaker, who does not have hearing issues, seeing
sub-titles like
```
[Groovy music playing]
```
kinda messes with my immersion. So this thing SHOULD nicely strip
out every subtitle, that's wrapped in brackets.

## Usage
```
$ un-hohher foo.srt
```

This outputs `test-input.un-hohhed.srt` in the current directory.

The parameter is required, or the program will terminate with error code 1.

## Current limitations

The matching is quite dumb, just checking wrapping with `[` and `]`.

Complex False Positives like
```
[Ale] Sini?
[Lare] Cini, C:llä?
[Confused Ale noises]
```
are not recognized yet.
