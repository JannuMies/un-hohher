package main

import (
	"fmt"
	"os"
	"regexp"
	"strings"
)

const inputFile = "./testResources/test-input.srt"
const outputFileName = "./test-input.un-hohhed.srt"

func maybePanic(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	argLength := len(os.Args)
	if argLength < 2 {
		fmt.Printf("No arguments given, file is required.\n")
		fmt.Printf("Use 'un-hohher foo.srt'\n")
		os.Exit(1)
	}

	doTheMagic(os.Args[1])
}

func doTheMagic(inputFileName string) {
	dat, err := os.ReadFile(inputFileName)
	maybePanic(err)

	asString := string(dat)
	singleTextLines := splitByNewlines(asString)
	textLinesWithoutEmpties := filterEmpties(singleTextLines)
	subLines := toSubLines(textLinesWithoutEmpties)
	filteredSubLines := filterEffects(subLines)
	outputStrings := concatenateToString(filteredSubLines)
	combinedOutput := strings.Join(outputStrings, "\n\n")
	_ = combinedOutput // TODO: Nuke

	file, err := os.Create(outputFileName)
	maybePanic(err)
	file.WriteString(combinedOutput)
	file.Close()
}

func toSubLines(stringSlice []string) []SubLine {
	result := make([]SubLine, len(stringSlice))
	for i, item := range stringSlice {
		result[i] = parseString(item)
	}
	return result
}

func parseString(s string) SubLine {
	asSlice := strings.Split(s, "\n")
	length := len(asSlice)
	if length < 3 {
		fmt.Printf("Slice is too short: %v", s)
		os.Exit(2)

	}
	return SubLine{
		OriginalNumber: asSlice[0],
		TimeCode:       asSlice[1],
		Text:           asSlice[2:length],
	}

}

func splitByNewlines(s string) []string {
	replaced := regexp.
		MustCompile("\r\n").
		ReplaceAllString(s, "\n")

	return regexp.
		MustCompile(`\n\s*\n`).
		Split(replaced, -1)
}

func filterEmpties(lines []string) []string {
	filtered := []string{}
	for i := range lines {
		if len(lines[i]) > 0 {
			filtered = append(filtered, lines[i])
		}
	}
	return filtered

}

func filterEffects(subLines []SubLine) []SubLine {
	filtered := []SubLine{}
	for i := range subLines {
		if !subLines[i].isEffect() {
			filtered = append(filtered, subLines[i])
		}
	}
	return filtered
}

func concatenateToString(subLines []SubLine) []string {
	subLinesAsString := []string{}
	for i := range subLines {
		asString := subLines[i].toString(i + 1)
		subLinesAsString = append(subLinesAsString, asString)
	}
	return subLinesAsString
}
