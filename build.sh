#!/bin/bash
exit_if_error() {
  local exit_code=$1
  shift
  [[ $exit_code ]] &&
    ((exit_code != 0)) && {
      printf 'ERROR: %s\n' "$@" >&2
      exit "$exit_code"
    }
}

go test
exit_if_error $? "Tests failed. *tableflip*"

go build
exit_if_error $? "Linux build failed, whattaa?"

export GOOS=windows 
export GOARCH=amd64 
export CGO_ENABLED=1 
export CC=i586-mingw32msvc-gcc 
go build -o ${PWD##*/}.exe -ldflags "-extldflags -static"
exit_if_error $? "Windows build failed, blergh..."
